package com.vsb.kru13.osmzhttpserver;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SocketServer extends Thread {

    /* Properties */

    private final int port = 12345;
    private ServerSocket serverSocket;
    private boolean bRunning;

    private final Handler logHandler;
    private final Semaphore semaphore;

    private final Handler threadUsageHandler;

    private List<HttpHandler> activeHandlers = new ArrayList<>();

    private byte[] cameraData;
    public byte[] getCameraData() {
        return cameraData;
    }
    public void setCameraData(byte[] cameraData) {
        this.cameraData = cameraData;

        List<HttpHandler> inactive = new ArrayList<>(activeHandlers.size());
        for (HttpHandler httpHandler : activeHandlers) {
            if (httpHandler.isAlive()) {
                try {
                    httpHandler.getCameraDataQueue().put(cameraData);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    inactive.add(httpHandler);
                }
            } else {
                inactive.add(httpHandler);
            }
        }
        activeHandlers.removeAll(inactive);
    }

    /* Initialization */

    SocketServer(@NonNull Handler logHandler, int threads, Handler threadUsageHandler) {
        this.logHandler = logHandler;
        semaphore = new Semaphore(threads);
        this.threadUsageHandler = threadUsageHandler;
    }

    /* Thread */

    public void run() {
        try {
            Log.d("SERVER", "Creating Socket");
            serverSocket = new ServerSocket(port);
            bRunning = true;

            log("Server started");

            while (bRunning) {
                Log.d("SERVER", "Socket Waiting for connection");
                Socket s = serverSocket.accept();
                Log.d("SERVER", "Socket Accepted");

                // TODO try out blocking acquire
                if (semaphore.tryAcquire()) {
                    Log.d("SERVER", "Handler thread allocated");

                    Message msg = threadUsageHandler.obtainMessage();
                    msg.arg1 = semaphore.availablePermits();
                    msg.sendToTarget();

                    HttpHandler httpHandler = new HttpHandler(this, s, logHandler, semaphore, threadUsageHandler);
                    activeHandlers.add(httpHandler);
                    httpHandler.start();
                }
                else {
                    Log.d("SERVER", "Server too busy, socket discarded");
                    log("Server too busy, socket discarded");
                    tooBusyResponse(s);
                    s.close();
                }
            }
        } catch (IOException e) {
            if (serverSocket != null && serverSocket.isClosed()) {
                Log.d("SERVER", "Normal exit");
                log("Server stopped");
            } else {
                Log.d("SERVER", "Error in server");
                log("Server error");
                e.printStackTrace();
            }
        } finally {
            serverSocket = null;
            bRunning = false;
        }
    }

    void close() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            Log.d("SERVER", "Error, probably interrupted in accept(), see log");
            log("Server error, probably interrupted");
            e.printStackTrace();
        }
        for (HttpHandler httpHandler : activeHandlers) {

        }
        activeHandlers.clear();
        bRunning = false;
    }

    /* Helpers */

    private void log(String text) {
        Message msg = logHandler.obtainMessage();
        msg.obj = text;
        msg.sendToTarget();
    }

    private void tooBusyResponse(Socket s) {
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            out.write("HTTP/1.0 503 Service Unavailable\n");
            out.write("Content-Type: text/plain\n\n");
            out.write("Server too busy, try later\n");
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

