package com.vsb.kru13.osmzhttpserver;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpHandler extends Thread {

    /* Properties */

    private final SocketServer server;
    private final Socket socket;
    private OutputStream o;
    private BufferedWriter out;
    private BufferedReader in;

    private final Handler logHandler;
    private final Semaphore semaphore;

    private final Handler threadUsageHandler;

    private final ArrayBlockingQueue<byte[]> cameraDataQueue = new ArrayBlockingQueue<>(5);

    ArrayBlockingQueue<byte[]> getCameraDataQueue() {
        return cameraDataQueue;
    }

    /* Initialization */

    HttpHandler(SocketServer server, Socket socket, Handler logHandler, Semaphore semaphore, Handler threadUsageHandler) {
        this.server = server;
        this.socket = socket;
        this.logHandler = logHandler;
        this.semaphore = semaphore;
        this.threadUsageHandler = threadUsageHandler;
    }

    /* Run */

    @Override
    public void run() {
        try {
            o = socket.getOutputStream();
            out = new BufferedWriter(new OutputStreamWriter(o));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String uri = getURI();
            if (uri == null) {
                error400("Malformed URI");
                return;
            }

            File file = getTargetFile(uri);
            if (uri.equals("/camera/snapshot")) {
                serveSnapshot();
            } else if (uri.equals("/camera/stream")) {
                streamMJPEG();
            } else if (uri.startsWith("/cgi-bin/")) {
                cgi(uri.substring("/cgi-bin/".length()));
            } else if (file.exists() && file.isFile()) {
                serveFile(file, uri);
            } else {
                error404(uri);
            }
        } catch (IOException e) {
            Log.d("HTTP HANDLER", "Error handling request");
            e.printStackTrace();
        } finally {
            try {
                socket.close();
                o.close();
                out.close();
                in.close();
                Log.d("HTTP HANDLER", "Socket Closed Successfully");
            } catch (IOException e) {
                Log.d("HTTP HANDLER", "Socket Closed with Errors");
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.d("HTTP HANDLER", "Socket Closed with Errors");
                e.printStackTrace();
            }
            semaphore.release();

            Message msg = threadUsageHandler.obtainMessage();
            msg.arg1 = semaphore.availablePermits();
            msg.sendToTarget();
        }
    }

    /* Helpers */

    private void error400(String message) throws IOException {
        out.write("HTTP/1.0 400 Bad Request\n");
        out.write("Content-Type: text/plain\n");
        out.write("\n");
        out.write("400 Bad Request\n");
        if (message != null) {
            out.write(message);
        }
        out.write("\n");
        out.flush();

        logRequest("", 0, 400);
    }

    private void error404(String uri) throws IOException {
        out.write("HTTP/1.0 404 Not Found\n");
        out.write("Content-Type: text/plain\n");
        out.write("\n");
        out.write("404 Not Found\n");
        if (uri != null) {
            out.write("URI not found: " + uri);
        }
        out.write("\n");
        out.flush();

        logRequest(uri, 0, 404);
    }

    private String getURI() throws IOException {
        String uri = null;
        for (String line = in.readLine(); line != null && !line.equals(""); line = in.readLine()) {
            Log.d("SERVER", "HTTP REQ " + line);
            if (uri == null) {
                Pattern pattern = Pattern.compile("^GET (\\S*) HTTP/1\\.[01]$");
                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    uri = matcher.group(1);
                }
            }
        }
        return uri;
    }

    private File getTargetFile(String uri) {
        File storage = Environment.getExternalStorageDirectory();
        File wwwRoot = new File(storage, "www");
        File file = new File(wwwRoot, uri);

        if (file.exists() && file.isDirectory()) {
            file = new File(file, "index.html");
        }

        Log.d("SERVER", "requested: " + file);
        return file;
    }

    private void serveFile(File file, String uri) throws IOException {
        fileHeaders(file);
        fileContents(file);

        logRequest(uri, file.length(), 200);
    }

    private void fileHeaders(File file) throws IOException {
        out.write("HTTP/1.0 200 Ok\n");

        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(file.getAbsolutePath());
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }

        if (type != null) {
            out.write("Content-Type: " + type + "\n");
        } else {
            out.write("Content-Type: text/plain\n");
        }

        out.write("Content-Length: " + file.length() + "\n");
        out.write("\n");
        out.flush();
    }

    private void fileContents(File file) throws IOException {
        InputStream fileInput = new BufferedInputStream(new FileInputStream(file));
        int b;
        while ((b = fileInput.read()) != -1) {
            o.write(b);
        }
        o.flush();
        fileInput.close();
    }

    private void logRequest(String uri, long length, int responseCode) {
        Bundle bundle = new Bundle();
        bundle.putString("uri", uri);
        bundle.putInt("response", responseCode);
        bundle.putLong("length", length);
        bundle.putLong("timestamp", System.currentTimeMillis());

        Message message = logHandler.obtainMessage();
        message.setData(bundle);
        message.sendToTarget();
    }

    private void serveSnapshot() throws IOException {
        // TODO maybe lock somehow
        byte[] cameraData = server.getCameraData();
        if (cameraData == null) {
            error404("/camera/snapshot");
            return;
        }

        out.write("HTTP/1.0 200 Ok\n");
        out.write("Content-Type: image/jpeg\n");
        out.write("Content-Length: " + cameraData.length + "\n");
        out.write("\n");
        out.flush();

        o.write(cameraData);
        o.flush();

        logRequest("/camera/snapshot", cameraData.length, 200);
    }

    private void streamMJPEG() throws IOException {
        out.write("HTTP/1.0 200 Ok\n");
        out.write("Content-Type: multipart/x-mixed-replace; boundary=\"mjpeg_boundary\"\n");
        out.write("\n");
        out.write("--mjpeg_boundary\n");
        out.flush();

        long total = 0;
        while (true) {
            try {
                byte[] cameraData = cameraDataQueue.take();
                try {
                    out.write("Content-Type: image/jpeg\n");
                    out.write("Content-Length: " + cameraData.length + "\n");
                    out.write("\n");
                    out.flush();

                    o.write(cameraData);
                    o.flush();

                    total += cameraData.length;

                    out.write("--mjpeg_boundary\n");
                    out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }

        Log.d("SERVER", "Finished the blocking queue!");
        logRequest("/camera/stream", total, 200);
    }

    private void cgi(String command) throws IOException {
        command = Uri.decode(command);
        Log.d("SERVER", "CGI: " + command);

        Process p = Runtime.getRuntime().exec(command);

        out.write("HTTP/1.0 200 Ok\n");
        out.write("Content-Type: text/plain\n");
        out.write("\n");
        out.flush();

        InputStream r = p.getInputStream();
        long total = 0;
        int n;
        byte[] buffer = new byte[1024];
        while ((n = r.read(buffer)) > -1) {
            o.write(buffer, 0, n);
            total += n;
        }

        o.flush();
        r.close();

        logRequest("/cgi-bin: " + command, total, 200);
    }

}
