package com.vsb.kru13.osmzhttpserver;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int READ_EXTERNAL_STORAGE = 1;
    private static final int WRITE_EXTERNAL_STORAGE = 2;
    private static final int CAMERA = 3;

    private SocketServer s;
    private Handler logHandler;
    private long totalLength = 0;

    private EditText threadLimit;
    private Handler threadUsageHandler;

    /* Camera Functionality */
    private Camera mCamera;
    private MediaRecorder mediaRecorder;
    private Lock cameraLock = new ReentrantLock();
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            mCamera.stopPreview();
//            File pictureFile = new File(wwwRoot, "camera.jpg");
//            try {
//                FileOutputStream fos = new FileOutputStream(pictureFile);
//                fos.write(data);
//                fos.close();
//            } catch (FileNotFoundException e) {
//                Log.d("MAIN", "File not found: " + e.getMessage());
//            } catch (IOException e) {
//                Log.d("MAIN", "Error accessing file: " + e.getMessage());
//            }
//            finally {
//                mCamera.startPreview();
//            }

            if (s != null) {
                s.setCameraData(data);
            }

            mCamera.startPreview();
            cameraLock.unlock();
        }
    };
    private static int CAPTURE_INTERVAL = 1000;
    private Handler timedCapture = new Handler();
    private Camera.Parameters cameraParams;
    private List<Camera.Size> pictureSizes;
    private boolean autoCapture = false;

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    private void initCameraPreview() {
        // Create an instance of Camera
        mCamera = getCameraInstance();
        if (mCamera == null) {
            return;
        }

        // Create our Preview view and set it as the content of our activity.
        CameraPreview mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = findViewById(R.id.camera_preview);
        preview.addView(mPreview);

        // Select picture size
        cameraParams = mCamera.getParameters();
        pictureSizes = cameraParams.getSupportedPictureSizes();
        List<String> sizes = new ArrayList<>(pictureSizes.size());
        for (Camera.Size size : pictureSizes) {
            sizes.add("" + size.width + "x" + size.height);
        }

        Spinner dropdown = findViewById(R.id.pictureSize);
        dropdown.setAdapter(new ArrayAdapter<>(
                this, R.layout.support_simple_spinner_dropdown_item, sizes
        ));

        Camera.Size initialSize = pictureSizes.get(dropdown.getSelectedItemPosition());
        cameraParams.setPictureSize(initialSize.width, initialSize.height);
        mCamera.setParameters(cameraParams);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Camera.Size selectedSize = pictureSizes.get(position);
                cameraParams.setPictureSize(selectedSize.width, selectedSize.height);
                mCamera.setParameters(cameraParams);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Nothing
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn1 = findViewById(R.id.button1);
        Button btn2 = findViewById(R.id.button2);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);

        final TextView logView = findViewById(R.id.logView);
        final TextView totalView = findViewById(R.id.totalView);
        totalView.setText(Long.toString(totalLength));

        logHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                Bundle bundle = msg.getData();
                if (bundle.getLong("timestamp") != 0) {
                    String uri = bundle.getString("uri", "");
                    int responseCode = bundle.getInt("response", 0);
                    long timestamp = bundle.getLong("timestamp", 0);
                    long length = bundle.getLong("length", 0);

                    logView.append(timestamp + ";\t" + responseCode + ";\t" + uri + ";\t" + length + "B\n");

                    totalLength += length;
                    totalView.setText(Long.toString(totalLength) + " B");
                } else {
                    logView.append(msg.obj + "\n");
                }
            }
        };

        threadLimit = findViewById(R.id.threadLimit);
        final EditText threadUsageView = findViewById(R.id.threadUsage);
        threadUsageHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                int available = msg.arg1;
                int threadLimitCount = Integer.parseInt(threadLimit.getText().toString());
                int threadUsage = threadLimitCount - available;
                threadUsageView.setText(Integer.toString(threadUsage));
            }
        };

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.CAMERA}, CAMERA
            );
        } else {
            initCameraPreview();
        }

        Button captureButton = findViewById(R.id.button_capture);
        final AppCompatActivity self = this;
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("SERVER", "Pressed capture!");
                        int permissionCheck = ContextCompat.checkSelfPermission(
                                self, Manifest.permission.WRITE_EXTERNAL_STORAGE
                        );
                        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(
                                    self, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    WRITE_EXTERNAL_STORAGE
                            );
                        } else {
                            // get an image from the camera
                            cameraLock.lock();
                            mCamera.takePicture(null, null, mPicture);
                        }
                    }
                }
        );

        findViewById(R.id.autoCapture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoCapture = ((CheckBox) v).isChecked();
                if (autoCapture) {
                    timedCapture.post(new Runnable() {
                        @Override
                        public void run() {
                            if (autoCapture) {
                                cameraLock.lock();
                                mCamera.takePicture(null, null, mPicture);
                                timedCapture.postDelayed(this, CAPTURE_INTERVAL);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
    }

    @Override
    protected void onResume() {
        super.onResume();
        initCameraPreview();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1: {
                int permissionCheck = ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                );
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            READ_EXTERNAL_STORAGE
                    );
                } else {
                    startSocketServer();
                }
                break;
            }
            case R.id.button2: {
                stopSocketServer();
                break;
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_EXTERNAL_STORAGE: {
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startSocketServer();
                }
                break;
            }
            case CAMERA: {
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    initCameraPreview();
                }
                break;
            }
            case WRITE_EXTERNAL_STORAGE:
                break;
        }
    }

    private void startSocketServer() {
        int threadLimitCount = Integer.parseInt(this.threadLimit.getText().toString());
        threadLimit.setEnabled(false);

        s = new SocketServer(logHandler, threadLimitCount, threadUsageHandler);
        s.start();
    }

    private void stopSocketServer() {
        s.close();
        try {
            s.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s = null;
        threadLimit.setEnabled(true);
    }

}
